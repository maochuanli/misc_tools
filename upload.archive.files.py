#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
/mnt2/cdic/archive/skinny/  --> s3://skinny-production-dwh-landing-bucket/skinny/archive/skinny
/mnt2/cdic/archive/zoho     --> s3://skinny-production-dwh-landing-bucket/skinny/archive/zoho
/mnt2/cdic/archive/zuora    --> s3://skinny-production-dwh-landing-bucket/skinny/archive/zuora

All files need be uploaded to S3 bucket
"""

start_date_str = '2017/05/01'

work_items = [
    ['/mnt2/cdic/archive/skinny/', 's3://skinny-production-dwh-landing-bucket/skinny/archive/skinny/'],
    ['/mnt2/cdic/archive/zoho/', 's3://skinny-production-dwh-landing-bucket/skinny/archive/zoho/'],
    ['/mnt2/cdic/archive/zuora/', 's3://skinny-production-dwh-landing-bucket/skinny/archive/zuora/'],
]

import os
import time
import subprocess


def get_files_later_than(dir, later_datetime_in_seconds):
    """
    Get a list of the file names under the directory
    :param dir: the local archive directory
    :param later_datetime_in_seconds: the starting date in time.struct_time type
    :return: a list of file names later than the given date
    """
    all_files = os.listdir(dir)

    sub_files = filter(lambda f: os.stat(os.path.join(dir, f)).st_ctime >= later_datetime_in_seconds, all_files)
    return sub_files


def run_cmd(cmd):
    # print('cmd: ', cmd)
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    result = process.communicate()[0]
    return process.returncode, result


def get_s3_files(bucket_dir):
    cmd = 'aws s3 ls ' + bucket_dir
    rc, output = run_cmd(cmd)
    lines = output.split('\n')
    s3_files = set()

    for line in lines:
        line_segs = line.split()
        if len(line_segs) == 4:
            s3_files.add(line_segs[-1])

    return s3_files


def upload_file_to_s3(local_file, s3_bucket_dir):
    cmd = 'aws s3 cp {0} {1} --sse'.format(local_file, s3_bucket_dir)
    return run_cmd(cmd)


if __name__ == "__main__":
    start_date = time.strptime(start_date_str, "%Y/%m/%d")
    start_date_in_seconds = time.mktime(start_date)
    for item in work_items:
        local_dir = item[0]
        s3_dir = item[1]
        if not s3_dir.endswith('/'):
            s3_dir += '/'
        local_file_names = get_files_later_than(local_dir, start_date_in_seconds)
        remote_file_names = get_s3_files(s3_dir)
        for f in local_file_names:
            if f not in remote_file_names:
                code, out = upload_file_to_s3(os.path.join(local_dir, f), s3_dir)
                print(code, out)
