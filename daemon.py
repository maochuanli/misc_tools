import os, sys, time


def daemon_start():
    '''This is the main function run by the daemon.
    This just writes the process id to a file then
    sits in a loop and writes numbers to the file once a second.
    You could also put an exec in here to switch to a different program.
    '''
    fout = open ('daemon', 'a')
    fout.write ('daemon started with pid %d\n' % os.getpid())
    c = 0
    while 1:
        fout.write ('Count: %d\n' % c)
        fout.flush()
        c = c + 1
        time.sleep(1)


if __name__ == "__main__":
    #
    # This is interesting section that does the daemon magic.
    #
    pid = os.fork ()
    if pid == 0: # if pid is child
        os.setsid() # Start new process group.
        pid = os.fork () #Second fork will start detatched process.
        if pid == 0: # if pid is child
            daemon_start ()