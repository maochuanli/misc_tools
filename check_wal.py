start_file_name = '000000030000634500000094' #Wed Oct 24 15:13:38 NZDT 2018
end_file_name = '0000000300006347000000FB'   #Wed Oct 24 15:27:59 NZDT 2018

start_block_num = int(start_file_name[0:16], 16)
start_seg_num = int(start_file_name[16:24], 16)

end_block_num = int(end_file_name[0:16], 16)
end_seg_num = int(end_file_name[16:24], 16)

print hex(end_block_num), hex(end_seg_num)

total_file_num = (end_block_num - start_block_num) * 256 + end_seg_num - start_seg_num + 1

if __name__ == "__main__":
    print total_file_num

