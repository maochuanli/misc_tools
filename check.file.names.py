#!/usr/bin/env python

import logging, getopt, traceback
import os
import socket
import sys
import time
import subprocess
import zipfile

# if len(sys.argv) < 3:
#     print('please run this script with parameters: list_file_name, start_wal_file_name, [end_wal_file_name]')
#     exit(-1)


# run the aws command to get the latest wa file list
# aws s3 ls s3://skinny-production-dwh-landing-bucket/skinny/wal/crce/ --profile skinny
def run_cmd(cmd):
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    result = process.communicate()[0]
    return result


def check_wal_files(db_name, start_file_name, end_file_name):

# db_name = sys.argv[1]
# start_file_name = sys.argv[2]
# end_file_name = None

    file_list_out = run_cmd('aws s3 ls s3://skinny-production-dwh-landing-bucket/skinny/wal/{}/ --profile skinny'.format(db_name))
    file_lines = file_list_out.split('\n')

    if len(sys.argv) > 3:
        end_file_name = sys.argv[3]

    # if not os.path.exists(file_name):
    #     print('list_file not exists error: ' + file_name)
    #     exit(-2)

    if len(start_file_name) != 24:
        print('start wal file name must be of length 24: ' + start_file_name)
        exit(-3)

    if end_file_name and len(start_file_name) != 24:
        print('end wal file name must be of length 24: ' + start_file_name)
        exit(-4)

    start_block_num = int(start_file_name[0:16], 16)
    start_seg_num = int(start_file_name[16:], 16)

    left = hex(start_block_num).lstrip("0x").upper().rjust(16, '0')
    right = hex(start_seg_num).lstrip("0x").upper().rjust(8, '0')

    found_file_list = set()

    # with open(file_name, 'r') as f:
    #     lines = f.readlines()
    for line in file_lines:
        line_file_name = None
        line_seg_list = line.split()
        for seg in line_seg_list:
            if seg.endswith('.gz'):
                line_file_name = seg.strip()
        # print('found line file name is: ' + line_file_name)
        if not line_file_name:
            print('skip an  invalid file name: ' + line)
            continue
        found_file_list.add(line_file_name)
        last_file_name_in_file = line_file_name

    if not end_file_name:
        end_file_name = last_file_name_in_file

    print('start file name: {}{}'.format(left, right))
    print('end file name: {}'.format(end_file_name))

    end_block_num = int(end_file_name[0:16], 16)
    end_seg_num = int(end_file_name[16:24], 16)

    print hex(end_block_num), hex(end_seg_num)

    total_file_num = (end_block_num - start_block_num) * 256 + end_seg_num - start_seg_num + 1
    print 'total file num is: ' + str(total_file_num)

    # for f in found_file_list:
    #	print('show found file name: [{}]'.format(f))

    log = logging.getLogger('root')

    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

    num_miss_files = 0

    miss_file_item = list()

    for i in range(total_file_num):
        # logging.info('process {}'.format(i))
        left = hex(start_block_num).lstrip("0x").upper().rjust(16, '0')
        right = hex(start_seg_num).lstrip("0x").upper().rjust(8, '0')
        tmp_file = left + right + '.gz'
        # logging.info('try find: ' + tmp_file)
        # print ('tmp file: ' + tmp_file)
        if not tmp_file in found_file_list:
            # print (tmp_file)
            miss_file_item.append({'index': i, 'name': tmp_file})
            num_miss_files += 1
        # logging.info("after found")
        start_seg_num += 1
        if start_seg_num >= 256:
            start_block_num += 1
            start_seg_num -= 256
        # logging.info("next round..")

    print('There are {} missing files'.format(num_miss_files))
    for index in range(len(miss_file_item) - 2):
        start_item = miss_file_item[index]
        start_item_index = start_item.get('index')
        next_item = miss_file_item[index + 1]
        next_item_index = next_item.get('index')
        next_next_item = miss_file_item[index + 2]
        next_next_item_index = next_next_item.get('index')

        if next_item_index == start_item_index + 1 and next_next_item_index == next_item_index + 1:
            next_item['canSkip'] = True

    for item in miss_file_item:
        if item.get('canSkip') == True:
            print(item.get('name') + ' --')
        else:
            print(item.get('name'))


if __name__ == "__main__":
    # extract_xls_file()
    check_wal_files('crce', '000000040000756B00000099', None)