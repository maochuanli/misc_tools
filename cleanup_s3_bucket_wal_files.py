#!/usr/bin/env python

import logging
import logging.handlers
import getopt
import os
import sys
import fcntl
import subprocess
from multiprocessing.pool import ThreadPool

S3_CMD_LS = "aws --profile skinny s3 ls"
S3_CMD_MV = "aws --profile skinny s3 mv"
S3_CMD_LS = "aws s3 ls"
S3_CMD_MV = "aws s3 mv"

fp = open('/tmp/s3.cleanup.lock', 'w')


def lock_file():
    try:
        fcntl.flock(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        # print('io error')
        return False

    # print('no error, good to go')
    return True


# def unlock_file():
#     try:
#         fcntl.flock(fp, fcntl.LOCK_UN)
#     except IOError:
#         return False
#
#     return True


def run_cmd(cmd):
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    result = process.communicate()[0]
    return result


def loop_cleanup_and_create_dummy(logger, last_wal_restore_point, log_dir, s3_bucket_uri_from,
                                  s3_bucket_uri_to):
    # with open(postgres_last_ingested_wal_file, 'r') as f:
    #     last_wal_restore_point = f.read().strip()
    #
    # if not last_wal_restore_point:
    #     logger.error("Could not get the last restore point value from file: " + postgres_last_ingested_wal_file)

    ingested_file_list = []
    uningested_file_list = []

    ls_cmd_out = run_cmd('{} {}'.format(S3_CMD_LS, s3_bucket_uri_from))
    cmd_out_lines = ls_cmd_out.split('\n')
    for line in cmd_out_lines:
        line = line.strip()
        # logger.info('daemon: ' + line)
        if len(line) > 0:
            line_seg_lists = line.split()
            file_name = line_seg_lists[-1]
            if file_name.endswith('.gz'):
                if file_name < last_wal_restore_point:
                    # cmd = '{} {}{} {}{} --sse=AES256'.format(S3_CMD_MV, s3_bucket_uri_from, file_name, s3_bucket_uri_to, file_name)
                    # mv_cmd_out = run_cmd(cmd)
                    # logger.info('move file: ' + file_name)
                    ingested_file_list.append(file_name)
                else:
                    uningested_file_list.append(file_name)
            else:
                logger.warn('unrecognized: ' + line)

    # Delete all files under the work directory
    dummy_file_list = os.listdir(log_dir)
    for dummy in dummy_file_list:
        if dummy.endswith('.gz'):
            os.remove(os.path.join(log_dir, dummy))

    # Create all dummy files
    logger.info("To create {} dummy files!".format(len(uningested_file_list)))
    for f in uningested_file_list:
        if f.endswith('.gz'):
            # logger.info('create file: ' + f)
            dummy = open(os.path.join(log_dir, f), 'w')
            dummy.close()

    mv_cmd_out_list = []

    if len(ingested_file_list) <= 0:
        logger.info("no ingested files!")
    else:
        # logger.info('let\'s move the ingested files to archive folder')
        # pool = multiprocessing.Pool(processes=5)
        tPool = ThreadPool(processes=5)
        for f in ingested_file_list:
            logger.info('move file: ' + f)
            cmd = '{} {}{} {}{} --sse=AES256'.format(S3_CMD_MV, s3_bucket_uri_from, f, s3_bucket_uri_to, f)
            pOUT = tPool.apply_async(run_cmd, args=(cmd,))
            mv_cmd_out_list.append(pOUT)

        for out in mv_cmd_out_list:
            output = out.get()
            logger.info(output)


def usage():
    a0 = sys.argv[0]
    print("NAME: ")
    print("    {} - AWS S3 bucket WAL files cleaner".format(a0))
    print("")
    print("SYNOPSIS")
    print("    {} [OPTIONS]".format(a0))
    print("")

    print("DESCRIPTION")
    print("    {} is \n"
          "    A python script for cleaning up the WAL files in S3 bucket after the files\n"
          "      has been successfully ingested by local postgresql database.\n"
          "    The program will also generate a list of dummy files with the same names as those in S3 bucket\n"
          "      that has not been ingested for Datadog monitoring.".format(a0))
    print("")
    print("    -r ")
    print("        The %r variable - the name of the file containing the last valid restart point")
    print("    -l ")
    print("        The Log directory where dummy wal files will be generated")
    print("    -f ")
    print("        The S3 bucket URI where WAL files are downloaded from")
    print("    -t ")
    print("        The S3 bucket URI where WAL files are to be archived to")
    print("")
    sys.exit(-1)


def daemon_start(log_dir, last_ingested_wal_file, s3_bucket_uri_from, s3_bucket_uri_to):
    if not lock_file():
        print('another instance running...')
        sys.exit(0)

    # Define a console logger
    logger = logging.getLogger('skinny_application')
    logger.setLevel(logging.INFO)
    # ch.setLevel(logging.ERROR)
    # formatter = logging.Formatter('%(asctime)s | %(levelname)8s | %(message)s')
    formatter = logging.Formatter('%(asctime)s | %(message)s')

    file_handler = logging.handlers.RotatingFileHandler(os.path.join(log_dir, "s3_wal_watcher.log"),
                                                        maxBytes=10 * 1024 * 1024, backupCount=5)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    logger.info("Postgres last valid restart point file:   {}".format(last_ingested_wal_file))
    logger.info("Log directory                         :   {}".format(log_dir))
    logger.info("S3 bucket URI from                    :   {}".format(s3_bucket_uri_from))
    logger.info("S3 bucket URI to                      :   {}".format(s3_bucket_uri_to))

    logger.info("")

    if not s3_bucket_uri_from.endswith('/'):
        s3_bucket_uri_from += '/'
    if not s3_bucket_uri_to.endswith('/'):
        s3_bucket_uri_to += '/'

    loop_cleanup_and_create_dummy(logger, last_ingested_wal_file, log_dir, s3_bucket_uri_from, s3_bucket_uri_to)


if __name__ == "__main__":

    s3_bucket_uri_from = None
    s3_bucket_uri_to = None
    log_dir = None
    last_ingested_wal_file = None

    try:
        opts, args = getopt.getopt(sys.argv[1:],
                                   "f:t:r:l:h",
                                   ["help"])
        for opt, arg in opts:
            if opt in ("-f",):
                s3_bucket_uri_from = arg
            elif opt in ("-t",):
                s3_bucket_uri_to = arg
            elif opt in ("-r",):
                last_ingested_wal_file = arg
            elif opt in ("-l",):
                log_dir = arg
            elif opt in ("-h", "--help"):
                usage()

    except getopt.GetoptError as err:
        print("Error parsing command line arguments ..." + str(err))
        usage()

    if not last_ingested_wal_file or not s3_bucket_uri_from or not s3_bucket_uri_to or not log_dir:
        usage()

    pid = os.fork()
    if pid == 0:  # if pid is child
        os.setsid()  # Start new process group.
        pid = os.fork()  # Second fork will start detatched process.
        if pid == 0:  # if pid is child
            daemon_start(log_dir, last_ingested_wal_file, s3_bucket_uri_from, s3_bucket_uri_to)
